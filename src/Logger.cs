using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public static class Logger
{
    public static List<string> ValidLevels = new List<string> { "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL" };

    public static string InvalidFileNameErrorMessage =
        "Invalid filename. Must end with .log and have between 1 and 20 letters in the name.";

    public static string InvalidMessageErrorMessage = "Invalid message. Must have between 1 and 50 characters.";

    public static string IvalidLogLevelErrorMessage =
        $"Invalid log level. Must be one of this: {string.Join(", ", ValidLevels)}";
    
    public static void Log(string filename, string message, string level)
    {
        if(string.IsNullOrWhiteSpace(filename) || !Regex.IsMatch(filename, @"\A[a-zA-Z]{1,20}\.log\z"))
        {
            throw new ArgumentException(InvalidFileNameErrorMessage);
        }

        if (string.IsNullOrWhiteSpace(message) || message.Length > 50)
        {
            throw new ArgumentException(InvalidMessageErrorMessage);
        }
        
        if (string.IsNullOrWhiteSpace(level) || !ValidLevels.Contains(level))
        {
            throw new ArgumentException(IvalidLogLevelErrorMessage);
        }
        
        string logEntry = $"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] [{level}] {message}\n";
        File.AppendAllText(filename, logEntry);
    }
}