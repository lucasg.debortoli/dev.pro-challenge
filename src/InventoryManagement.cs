using System;
using System.Collections.Generic;
using System.Linq;

public class InventoryManagement
{
    public static List<string> ValidKeys = new List<string> { "name", "price", "stock" };
    
    public static string InvalidProductListErrorMessage =
        "Invalid product list. It must have at least 1 element.";
    public static string InvalidProductErrorMessage =
        "Invalid product. All products must have name, price, stock and nothing else.";
    public static string InvalidProductNameErrorMessage =
        "Invalid product name. Must have between 1 and 50 characters.";
    
    public static string InvalidProductPriceErrorMessage =
        "Invalid product price. Must be a positive float.";
        
    public static string InvalidProductStockErrorMessage =
        "Invalid product stock. Must be a positive integer.";

    public static string InvalidSortKeyErrorMessage =
        "Invalid sortKey. Must be a name, price or stock.";
    
    public static List<Dictionary<string, object>> SortProducts(
        List<Dictionary<string, object>> products,
        string sortKey,
        bool ascending
    )
    {
        if (products is null || products.Count == 0)
        {
            throw new ArgumentException(InvalidProductListErrorMessage);
        }

        ValidateProducts(products);
        
        if (string.IsNullOrWhiteSpace(sortKey) || !ValidKeys.Contains(sortKey))
        {
            throw new ArgumentException(InvalidSortKeyErrorMessage);
        }

        List<Dictionary<string, object>> copiedList = new List<Dictionary<string, object>>(
            products.Select(dict => new Dictionary<string, object>(dict))
        );
        SelectionSort(copiedList, sortKey, ascending);
        return copiedList;
    }

    private static void ValidateProducts(List<Dictionary<string, object>> products)
    {
        foreach(Dictionary<string, object> product in products)
        {
            if (product is null || product.Count != 3)
            {
                throw new ArgumentException(InvalidProductErrorMessage);
            }

            foreach (string key in ValidKeys)
            {
                if (!product.ContainsKey(key))
                {
                    throw new ArgumentException(InvalidProductErrorMessage);
                }
            }

            if (!(product["name"] is string) || string.IsNullOrWhiteSpace((string)product["name"]) || ((string)product["name"]).Length > 50)
            {
                throw new ArgumentException(InvalidProductNameErrorMessage);
            }
            
            if (!(product["price"] is float) || (float)product["price"] < 0.0f)
            {
                throw new ArgumentException(InvalidProductPriceErrorMessage);
            }
            
            if (!(product["stock"] is int) || (int)product["stock"] < 0)
            {
                throw new ArgumentException(InvalidProductStockErrorMessage);
            }
        }
    }

    private static void SelectionSort(
        List<Dictionary<string, object>> products,
        string sortKey,
        bool ascending
        )
    {
        for (int i = 0; i < products.Count - 1; i++)
        {
            int extremeIndex = i;
            
            for (int j = i+1; j < products.Count; j++)
            {
                if (UpdateExtreme(products[extremeIndex], products[j], sortKey, ascending))
                {
                    extremeIndex = j;
                }
            }

            Swap(products, i, extremeIndex);
        }
    }

    private static bool UpdateExtreme(
        Dictionary<string, object> extreme,
        Dictionary<string, object> candidate,
        string sortKey,
        bool ascending
    )
    {
        IComparable extremeValue = (IComparable)extreme[sortKey];
        IComparable candidateValue = (IComparable)candidate[sortKey];

        if (ascending)
        {
            return extremeValue.CompareTo(candidateValue) > 0;
        }
        else
        {
            return extremeValue.CompareTo(candidateValue) < 0;
        }
    }
    
    private static void Swap(List<Dictionary<string, object>> products, int i, int j)
    {
        Dictionary<string, object> temp = products[i];
        products[i] = products[j];
        products[j] = temp;
    }
}