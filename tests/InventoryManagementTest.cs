using System;
using NUnit.Framework;

[TestFixture]
public class InventoryManagementTest
{
    public static IEnumerable<TestCaseData> InvalidSortProductsInputsTestCases
    {
        get
        {
            yield return new TestCaseData(null, "name", true, InventoryManagement.InvalidProductListErrorMessage);
            yield return new TestCaseData(new List<Dictionary<string, object>>(), "name", true, InventoryManagement.InvalidProductListErrorMessage);
            yield return new TestCaseData(
                new List<Dictionary<string, object>>() { new Dictionary<string, object> {} },
                "name", true, InventoryManagement.InvalidProductErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Invalid Product"}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Invalid Product"}}
                },
                "name", true, InventoryManagement.InvalidProductErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Invalid Product"}, {"price", 10.0f}, {"stock", 5}, {"new_key", 1}}
                },
                "name", true, InventoryManagement.InvalidProductErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"new_key", 1}}
                },
                "name", true, InventoryManagement.InvalidProductErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", 5}, {"price", 10.0f}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductNameErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", ""}, {"price", 10.0f}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductNameErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "    \n\n    "}, {"price", 10.0f}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductNameErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Message to test the code here with more than 50 characters"}, {"price", 10.0f}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductNameErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", false}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductPriceErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", 5}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductPriceErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", -5.0f}, {"stock", 5}}
                },
                "name", true, InventoryManagement.InvalidProductPriceErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", 5.0f}, {"stock", false}}
                },
                "name", true, InventoryManagement.InvalidProductStockErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", 5.0f}, {"stock", -1}}
                },
                "name", true, InventoryManagement.InvalidProductStockErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", 5.0f}, {"stock", 1}}
                },
                null, true, InventoryManagement.InvalidSortKeyErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", 5.0f}, {"stock", 1}}
                },
                "      ", true, InventoryManagement.InvalidSortKeyErrorMessage
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Valid name"}, {"price", 5.0f}, {"stock", 1}}
                },
                "invalidKey", true, InventoryManagement.InvalidSortKeyErrorMessage
            );
        }
    }

    public static IEnumerable<TestCaseData> ValidProductsInputsTestCases
    {
        get
        {
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }, "price", false,
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }, "price", true,
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}},
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}}
                }
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }, "stock", false,
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}},
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}}
                }
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }, "stock", true,
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }, "name", false,
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}}
                }
            );
            yield return new TestCaseData(
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }, "name", true,
                new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object> {{"name", "Product A"}, {"price", 100f}, {"stock", 5}},
                    new Dictionary<string, object> {{"name", "Product B"}, {"price", 200f}, {"stock", 3}},
                    new Dictionary<string, object> {{"name", "Product C"}, {"price", 50f}, {"stock", 10}}
                }
            );
        }
    }

    [TestCaseSource(nameof(InvalidSortProductsInputsTestCases))]
    public void InvalidSortProductsInputsShouldThrowArgumentException(
        List<Dictionary<string, object>> products,
        string sortKey,
        bool ascending,
        string excMsg
    )
    {
        ArgumentException exception = Assert.Throws<ArgumentException>(
            () => InventoryManagement.SortProducts(products, sortKey, ascending)
        );
        Assert.That(excMsg, Is.EqualTo(exception.Message));
    }
    
    
    [TestCaseSource(nameof(ValidProductsInputsTestCases))]
    public void ValidProductsShouldBeSortedCorrectly(
        List<Dictionary<string, object>> originalProducts,
        string sortKey,
        bool ascending,
        List<Dictionary<string, object>> expectedProducts
    )
    {
        List<Dictionary<string, object>> sorted = InventoryManagement.SortProducts(originalProducts, sortKey, ascending);
        Assert.That(expectedProducts.Count, Is.EqualTo(sorted.Count));

        for (int i = 0; i < expectedProducts.Count; i++)
        {
            foreach (var pair in expectedProducts[i])
            {
                Assert.IsTrue(sorted[i].ContainsKey(pair.Key));
                Assert.That(pair.Value, Is.EqualTo(sorted[i][pair.Key]));
            }
        }
    }
}
