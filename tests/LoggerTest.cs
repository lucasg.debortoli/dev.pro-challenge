using System;
using NUnit.Framework;
using System.Text.RegularExpressions;

[TestFixture]
public class LoggerTest
{  
    [TearDown]
    public void DeleteLogFiles()
    {
        string[] logFiles = Directory.GetFiles(".", "*.log");
        foreach (var file in logFiles)
        {
            File.Delete(file);
        }
    }    
    
    public static IEnumerable<TestCaseData> InvalidLogInputsTestCases
    {
        get
        {
            // Validate filename
            yield return new TestCaseData(null, "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("            ", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("\n\n\n\n\n\n", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("    \n\n    ", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData(".log", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData(" .log", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name .log", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name&.log", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name4.log", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("longNameWithMoreThan20Characters.log", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name.lo", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name.l", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name.", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            yield return new TestCaseData("name.g", "message", "INFO", Logger.InvalidFileNameErrorMessage);
            
            // Validate message
            yield return new TestCaseData("logFile.log", null, "INFO", Logger.InvalidMessageErrorMessage);
            yield return new TestCaseData("logFile.log", "", "INFO", Logger.InvalidMessageErrorMessage);
            yield return new TestCaseData("logFile.log", "    ", "INFO", Logger.InvalidMessageErrorMessage);
            yield return new TestCaseData("logFile.log", "\n\n\n\n", "INFO", Logger.InvalidMessageErrorMessage);
            yield return new TestCaseData("logFile.log", "  \n\n  ", "INFO", Logger.InvalidMessageErrorMessage);
            yield return new TestCaseData("logFile.log", "Message to test the logger with more than 50 characters", "INFO", Logger.InvalidMessageErrorMessage);
            
            // Validate level
            yield return new TestCaseData("logFile.log", "message", null, Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "    ", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "\n\n\n\n", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "  \n\n  ", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "invalid level", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "info", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "debug", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "warning", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "error", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "critical", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", " INFO", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "iNFO", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "INFO.", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", "INfO", Logger.IvalidLogLevelErrorMessage);
            yield return new TestCaseData("logFile.log", "message", ".", Logger.IvalidLogLevelErrorMessage);
        }   
    }
    
    public static IEnumerable<TestCaseData> ValidLogInputsTestCases
        {
            get
            {
                // Single low entry
                yield return new TestCaseData("application.log", "User logged in", "INFO", 1);
                yield return new TestCaseData("application.log", "Failed login attemp", "WARNING", 1);
                yield return new TestCaseData("application.log", "Failed login attemp", "DEBUG", 1);
                yield return new TestCaseData("application.log", "Failed login attemp", "ERROR", 1);
                yield return new TestCaseData("application.log", "Failed login attemp", "CRITICAL", 1);
                yield return new TestCaseData("logger.log", "    .    ", "WARNING", 1);
                yield return new TestCaseData("loggerFile.log", "Failed login attemp", "INFO", 1);
                yield return new TestCaseData("LOGGER.log", "\n\nmessage\n\n", "INFO", 1);
                yield return new TestCaseData("TheLogger.log", ".", "INFO", 1);

                // Multiple repetitions
                yield return new TestCaseData("application.log", "User logged in", "INFO", 6);
                yield return new TestCaseData("application.log", "User not logged in", "ERROR", 5);
                yield return new TestCaseData("application.log", "User logged in", "INFO", 10);
                yield return new TestCaseData("application.log", "User logged in", "INFO", 3);
            }   
        }

    [TestCaseSource(nameof(InvalidLogInputsTestCases))]
    public void InvalidLogInputsShouldThrowArgumentException(string filename, string msg, string level, string excMsg)
    {
        ArgumentException exception = Assert.Throws<ArgumentException>(
            () => Logger.Log(filename, msg, level)
        );
        Assert.That(excMsg, Is.EqualTo(exception.Message));
    }

    [TestCaseSource(nameof(ValidLogInputsTestCases))]
    public void ValidLogShouldSaveMessageOnFile(string filename, string msg, string level, int repetitions)
    {
        for(int i = 0; i < repetitions; i++)
        {
            Logger.Log(filename, msg, level);
        }
        ValidateContentFile(filename, msg, level, repetitions);
    }
  
    [Test]
    public void MultipleRepeatedLinesInDifferentFile()
    {
        Logger.Log("firstLogFile.log", "Message for logger 1", "ERROR");
        Logger.Log("secondLogFile.log", "Message for logger 2", "DEBUG");
        Logger.Log("thirdLogFile.log", "Message for logger 3", "INFO");
        
        ValidateContentFile("firstLogFile.log", "Message for logger 1", "ERROR", 1);
        ValidateContentFile("secondLogFile.log", "Message for logger 2", "DEBUG", 1);
        ValidateContentFile("thirdLogFile.log", "Message for logger 3", "INFO", 1);
    }
    
    private void ValidateContentFile(string filename, string msg, string level, int repetitions)
    {
        FileAssert.Exists(filename);
                
        string fileContent = File.ReadAllText(filename);
        string expected = @"^\[(?<datetime>\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\] \[" + Regex.Escape(level) + @"\] " + Regex.Escape(msg) + @"$";
        MatchCollection matches = Regex.Matches(fileContent, expected, RegexOptions.Multiline);

        Assert.That(matches.Count, Is.EqualTo(repetitions));
        foreach(Match match in matches)
         {
             GroupCollection groups = match.Groups;
             string date = groups["datetime"].Value;
             bool isValidDate = DateTime.TryParseExact(date, "yyyy-MM-dd HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out DateTime result);
             Assert.IsTrue(isValidDate);
         }
    }
}
